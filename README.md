Wallpapers
==========

Various wallpapers.
Main source are the different GNU/Linux distributions, and [Derek Taylor's (distrotube)](https://gitlab.com/dwt1) [own repository](https://gitlab.com/dwt1/wallpapers).

License
-------

It seems impossible to track the origin of most wallpapers.
If you would like credit/removal, please contact me!
